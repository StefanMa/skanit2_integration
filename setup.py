"""_____________________________________________________________________

:PROJECT: LARA

*lara_library setup *

:details: lara_library setup file for installation.
         - For installation, run:
           run pip3 install .
           or  python3 setup.py install

:file:    setup.py
:authors: 

:date: (creation)          
:date: (last modification) 

.. note:: -
.. todo:: - 
________________________________________________________________________
"""
__version__ = "0.0.4"

import os
import sys

from setuptools import setup, find_packages
#~ from distutils.sysconfig import get_python_lib

pkg_name = 'laralib'

def read(fname):
    try:
        return open(os.path.join(os.path.dirname(__file__), fname)).read()
    except IOError:
        return ''

install_requires = [] 
data_files = []
    
setup(name=pkg_name,
    version=__version__,
    description='LARA python library',
    long_description=read('README.rst'),
    author='mark doerr',
    author_email='mark.doerr@uni-greifswald.de',
    keywords='lab automation, experiments,LARA, database, evaluation, visualisation, SiLA2, robots',
    url='https://gitlab.com/LARAsuite/lara_library',
    license='GPL',
    packages=find_packages(), #['laralib'],
    #~ package_dir={'laralib':'laralib'},
    install_requires = install_requires,
    test_suite='',
    classifiers=['Development Status :: 1 - Development',
                   'Environment :: commandline',
                   'Framework :: python-django',
                   'Intended Audience :: Developers',
                   'License :: OSI Approved :: GPL',
                   'Operating System :: OS Independent',
                   'Programming Language :: Python',
                   'Programming Language :: Python :: 3.4',
                   'Programming Language :: Python :: 3.5',
                   'Topic :: Utilities'],
    include_package_data=True,
    data_files=data_files,
)
