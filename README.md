# lara_library

The LARAsuite python library for process control, data processing (evaluation and visualisation)

## Installation

```
python3 setup.py install
```

## Installation of required packages

    pip3 install --user -r requirements/devel.py

## Testing all applications

use this command to run all tests:

    python3 setup test
   
## tox


## Generating documentation

To generate the documentation, please change directory to app and run:

    sphinx-apidoc -o docs .
    make html
    

## Acknowledgements

The LARA-django developers thank 

    * the python team
    * the whole django team (https://www.djangoproject.com/) for their great tool !
    * [Django]( https://www.djangoproject.com/)
    

## References
    
[LARA]( https://gitlab.com/LARAsuite/)

[pip]( https://pypi.python.org/pypi/pip)
