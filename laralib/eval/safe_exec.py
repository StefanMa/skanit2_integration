"""_____________________________________________________________________

:PROJECT: lara_library

*lara_library safe exection of code*

:details: lara_library safe code execution

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20190114
:date: (last modification) 20190114

.. note:: -
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.0.3"

def safe_exec(code,namespace):
    """ :param [param_name]: [description]"""
    
    pass
