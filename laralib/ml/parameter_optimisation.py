"""
________________________________________________________________________

:PROJECT: LARA

*parameter_optimisation.py - starting parameter optimisation for machine learning *

:details: 
         - 
:file:    parameter_optimisation.py
:authors:  Stefan Born (stefan.born@tu-berlin.de)
           mark doerr  (mark@uni-greifswald.de)

:date: (creation)          20190626
:date: (last modification) 20190626

.. note:: -
.. todo:: - #    

________________________________________________________________________
"""
__version__ = "0.0.1"

import logging
import numpy as np
from sklearn.linear_model import Lasso, Ridge

from sklearn.model_selection import KFold,LeaveOneOut,RepeatedKFold
from sklearn.metrics import mean_squared_error, mean_absolute_error

def findOptimalRegularisation(X, y, alphas=[1e-6,1e-5,1e-4,1e-3,0.01,0.1,1,10,100,1e3,1e5,1e6], 
                              model_generator = Lasso):
    '''Trying to find a good/optimal regularisation parameter by trying
       different regularizations, storing all results in a dictionary of tuples.'''

    res_alpha_dic = {}
    
    min_activity = 1e-2 # minimal enzyme activity, to prevent negative activities during regression
    
    for alpha in alphas:
        curr_res_alpha = []
        """ Repeated K-Fold cross validator.
            Repeats K-Fold n times with different randomization in each repetition."""
        kfold = RepeatedKFold(5,n_repeats=100) # 4/5 of total

        #loo = LeaveOneOut() # overestimate of error
        for train_ind, test_ind in kfold.split(X):
            X_train = X[train_ind,:]
            y_train = y[train_ind]
            X_test = X[test_ind,:]
            y_test = y[test_ind]
            
            model = model_generator(alpha=alpha)
            
            # transformation into the logarithmic space, since multiplicative, not additive effects are expected
            model.fit(X_train,np.log(y_train+min_activity))
            y_pred_log = model.predict(X_test)
            
            rmse = np.sqrt(mean_squared_error(y_test,np.exp(y_pred_log) - min_activity))
            mae = mean_absolute_error(y_test,np.exp(y_pred_log)- min_activity)
            mae_log = mean_absolute_error(np.log( y_test + min_activity),y_pred_log)
            
            curr_res_alpha.append((rmse,mae,mae_log,model.coef_))
            
        res_alpha_dic[alpha] = curr_res_alpha 
    return res_alpha_dic
    
def summariseResults(results):
    """ summary of all results """
    alphas = sorted(results.keys())
    mae_means = []
    mae_stds = []
    mae_log_means = []
    mae_log_stds = []
    
    for a in alphas:
        maes = [r[1] for r in results[a]]
        maes_log = [r[2] for r in results[a]]
        mae_means.append(np.mean(maes))
        mae_stds.append(np.std(maes)/np.sqrt(len(maes)))
        mae_log_means.append(np.mean(maes_log))
        mae_log_stds.append(np.std(maes_log)/np.sqrt(len(maes_log)))
        
    return alphas, mae_means, mae_stds, mae_log_means, mae_log_stds


def print_tabular(alphas, means, stds):
    for a, m,s in zip(alphas, means, stds):
        print('alpha: {:<1.3e}, {:<2.5f} +- {:<2.5f}'.format(a,m,s))

def main():
    """ :param [param_name]: [description]"""
            
    coefs = []
    
    for i in range(500):
        subset = np.random.choice(range(X.shape[0]), 9*X.shape[0]//10,replace=False)
        model = Lasso(alpha=0.11)
        model.fit(X[subset,:],np.log(y[subset]+1e-2))
        coefs.append(model.coef_)
        
    mean_coefs = np.mean(coefs,axis=0)
    
    std_coefs = np.std(coefs,axis=0)/np.sqrt(len(coefs))
    
    mean_coefs, std_coefs
    
    for sn, c,s in zip(subst_names_1, mean_coefs,std_coefs):
        print('{:11s} {:>+1.5f} sigma= {:>+1.5f}'.format(sn,c,s))

if __name__ == '__main__':
    """Main: """

    alphas, means, stds, means_log, stds_log = sum_up_results(results)
    print_tabular(alphas,means, stds)
    print('------')
    print_tabular(alphas, means_log, stds_log)
    
    results_2 = try_out(alphas=np.arange(0.04,0.2,0.01))
    alpha, means, stds, means_log, stds_log = sum_up_results(results_2)
    print_tabular(alpha, means, stds)
    print('-----------')
    print_tabular(alpha, means_log, stds_log)
    
