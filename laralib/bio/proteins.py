"""
________________________________________________________________________

:PROJECT: LARA

*proteins.py - module for handling amino acid sequences*

:details: 
         - 
:file:    parameter_optimisation.py
:authors:  Stefan Born (stefan.born@tu-berlin.de)
           mark doerr  (mark@uni-greifswald.de)

:date: (creation)          20190707
:date: (last modification) 20190707

.. note:: -
.. todo:: - #    

________________________________________________________________________
"""
__version__ = "0.0.1"

import logging

from Bio.PDB.Polypeptide import one_to_three


def convMutLists2mutStr1LC(orig_aa,locs,subst_aa):
    """converts a three lists of original amino acids, loci and substituted amino acids 
       into a list of O_loc_S strings , e.g. ['G_143_A', ,,,], unsing one-letter amino acid code (1LC)
       :param [param_name]: [description]"""
       
    subst_names1L = [ o+'_'+l+'_'+s for (o,l,s) in zip(orig_aa,locs,subst_aa)]
       
    return subst_names1L

def convMutLists2mutStr3LC(orig_aa,locs,subst_aa):
    """converts a 
       into a list of ORG_loc_SUB strings, e.g. ['GLY_143_ALA', ...] , unsing three-letter amino acid code (3LC)
       :param [param_name]: [description]"""
       
    subst_names3L = [ one_to_three(o) + '_' + l +'_' + one_to_three(s) for (o,l,s) in zip(orig_aa,locs,subst_aa)]
       
    return subst_names3L
