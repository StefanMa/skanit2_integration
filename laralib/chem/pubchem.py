"""
________________________________________________________________________

:PROJECT: LARA

*pubchem.py - a chemistry information retrieval module*

:details: This module retrieves chemical data from pubchem databases
          Python interface for the PubChem PUG REST service.
          as described in:
          https://pubchemdocs.ncbi.nlm.nih.gov/pug-rest
          
          Some of the source is based upon Matt Swain's PubChemPy
          https://github.com/mcs07/PubChemPy
         - 
:file:    pubchem.py
:authors: mark doerr (mark@uni-greifswald.de)
          Matt Swain (m.swain@me.com)

:date: (creation)          20190307
:date: (last modification) 20190313

.. note:: -
.. todo:: - #    

________________________________________________________________________
"""
__version__ = "0.0.1"

import os
import sys
import tempfile
import glob
import time
import re
import json
import logging

#~ import warnings
#~ import binascii

from urllib.error import HTTPError
from urllib.parse import quote, urlencode
from urllib.request import urlopen

#~ from itertools import zip_longest

from os.path import expanduser
from configparser import ConfigParser, NoSectionError, NoOptionError

API_BASE = 'https://pubchem.ncbi.nlm.nih.gov/rest/pug'

def multi_replace(transl_dict, text):
    """ mulitple, dictionary based replacement
        s. https://www.oreilly.com/library/view/python-cookbook/0596001673/ch03s15.html
        :para transl_dict: replacement dictionary, e.g. {"a":"b", "alpha":"beta" }
        :para text: text to be replaced """
    
    # Create a regular expression from all of the dictionary keys
    regex = re.compile("|".join(map(re.escape, transl_dict.keys(  ))))
    
    # For each match, look up the corresponding value in the dictionary
    return regex.sub(lambda match: transl_dict[match.group(0)], text)
  
  
class SubstanceReaderPubChem():
    """ Class doc """
    def __init__ (self):
        """ Class initialiser """
        
        self.pubchem_cache_default = os.path.join( tempfile.gettempdir(), 'lara', 'pubchem'  )
        
        self.readConfigFile()
        
    def stripSubstanceName(self, substance_name):
        """This method generates a readable, short output name of the retrieved :param [param_name]: [description]"""
        
        transl_dict = { "-":"", "+":"" ,
                        "(":"", ")":"" ,
                        "meth":"me" ,
                        "eth":"et",
                        "prop":"pr",
                        "but":"bu",
                        "pent":"pe",
                        "hex":"hx",
                        "hept":"ht",
                        "oct":"oc",
                        "benz":"bz",
                        "phen":"ph",  }
        
        # remove all whitespaces before replacement
        return multi_replace(transl_dict, re.sub(r'\s+', '', substance_name) )

    def pubChemCompoundByName( self, substance_name='', acronym='', record_type='3d' ):
        """ pubChem import 
            s. http://pubchempy.readthedocs.io/en/latest/index.html  
              pubchem_link = 'http://pubchem.ncbi.nlm.nih.gov/summary/summary.cgi?sid={sid}'.format(sid=pubChemSubstanceID)
            safety: 
              https://pubchem.ncbi.nlm.nih.gov/rest/pug_view/data/compound/180/JSON/?response_type=save&response_basename=CID_180
            """
        
        short_subst_name = self.stripSubstanceName(substance_name)
        
        # check, if file with CAS is in cache:
        pubchem_json_file_list = glob.glob( os.path.join(self.pubchem_cache, r"*" + short_subst_name + "*.json") )
        
        if  len(pubchem_json_file_list) > 0 :
            for json_filename in pubchem_json_file_list:
                with open(json_filename, 'r') as json_file:
                    pubchem_subst = json.load(json_file)
                    
            logging.debug("pubchem subst: {}".format(pubchem_subst) )
            
        else : # retrieve data from PubChem database
            #~ pubchem_subst = pcp.get_json(substance_name, 'name' , )
            
            time.sleep(0.22)
            
            logging.debug("retriving compound from db: {}".format(substance_name) )
            
            ps = PubChem()
            
            #~ compound_lst = ps.get_compounds(substance_name, 'name', record_type='3d')
            
            pc_json = ps.get_json(substance_name, namespace="name", record_type='3d' )
            
            if pc_json:
                with open(os.path.join(self.pubchem_cache, short_subst_name + ".json"), 'w') as json_file:
                    json.dump(pc_json, json_file)
            
            logging.debug("~~~~~~comp list: {}".format(pc_json) )
            
            #~ for comp in compound_lst:
                #~ with open( os.path.join(self.pubchem_cache, "CID_" + str(comp.cid) 
                                                         #~ ##~ + "_CAS_"  + comp.cas 
                                                         #~ + "_" + short_subst_name + ".json" ), 'w' ) as json_file:
                    #~ json.dump(comp.record, json_file)
                
        return
        
        # if yes: load JSON

        #~ os.chdir(pubchem_cache)
        
        for json_filename in glob.glob( os.path.join(self.pubchem_cache, "*.json") ):
            
            m = re.match( pubchem_cache + r"CID_(\d+)_CAS_([\d,-]*).json", json_filename )
            
            try:
                self.substances_CID_set.add(m.groups()[0])
                self.substances_CAS_set.add(m.groups()[1])
          
            except AttributeError as err:
                logging.error(err)

        
        #~ if stripped_sn is in path:
            #~ with open( stripped_sn , 'r') as json_file:
                #~ pass
            # read json.json_file)
        
        # not in cache
        else: 
            # download from web
            
            pc_json = pcp.get_json(substance_name, 'name' , response_type="save", response_basename=stripped_sn)
            
            with open("CID_180.json", 'w') as json_file:
                json.dump(pc_json, json_file)
                        
            return
        
        for ele in self.substances_CAS_set:
            logging.debug(ele)
        
        if substance_name in self.substances_CAS_set:
            logging.info("substance already downloaded")
            
            # load json file ...
            #~ JSON_filename =  pubchem_cache + "CID_{0}_CAS_{1}.json".format(compound.cid, substance_name)
            #~ with open(JSON_filename, 'r') as json_file:
                    #~ json.load(json_file)
            
        else:
            logging.info("downloading subst info")
            compounds = pcp.get_compounds(substance_name, 'name' )
            
            #~ for param, sub_val in compounds[0].items() :
                    #~ print( " parameter: %s -->:%s\n" %(param, sub_val))
            
            for compound in compounds:
                
                substance = Substance(acronym=acronym)
                
                logging.debug(compound.synonyms[0])
                substance["name"] = compound.synonyms[0]
                substance["sumformula"] = compound.molecular_formula
                substance["CanSMILES"] = compound.canonical_smiles
                substance["InChI"] = compound.inchi
                substance["InChIKey"] = compound.inchikey #InChIKey.
                substance["PubChemID"] = compound.cid
                substance["PubChemFingerprint"] = compound.fingerprint
                #self.substance["InChI"] = compound.average_mass
                substance["mwt"] = compound.molecular_weight # Molecular weight.
                #self.substance["InChI"] = compound.monoisotopic_mass: Monoisotopic mass.
                #alogp: AlogP.
                substance["XlogP"] = compound.xlogp #: XlogP.
                substance["synonyms"] = compound.synonyms
                #~ substance["mol_raw"] = compound.mol_raw # Unprocessed MOL file.
                #~ substance["mol_2D"] = compound.mol_2d #: MOL file containing 2D coordinates.
                #~ substance["mol_3D"] = compound.mol_3d
                #~ substance["png"] = compound.image # 2D depiction as binary data in PNG format.
               
                self.substances.append(substance)
                
                # save JSON in cache
                
                JSON_filename =  pubchem_cache + "CID_{0}_CAS_{1}.json".format(compound.cid, substance_name)
                
                logging.debug(JSON_filename)
                
                with open(JSON_filename, 'w') as json_file:
                    json.dump(compound.record, json_file)
                    
            pubchem_substances = pcp.get_substances(substance_name, 'name')
            
            for pch_subst in pubchem_substances:
                
                JSON_filename =  pubchem_cache + "subst_CID_{0}_CAS_{1}.json".format(pch_subst.sid, substance_name)
                
                logging.debug(JSON_filename)
                
                #~ with open(JSON_filename, 'w') as json_file:
                    #~ json.dump(pch_subst.record, json_file)
            
    def readConfigFile(self):
        """ reading config file
           :param [param_name]: [description]"""
        
        config_file_missing = False
        cache_path_missing = False
        
        self.pubchem_cache = self.pubchem_cache_default
        
        config_dir = os.path.join( os.environ.get('APPDATA') or os.path.join(os.environ['HOME'], '.config', 'LARAsuite', ),  'PubChem' )
        
        if not os.path.exists(config_dir):
            os.makedirs(config_dir, exist_ok=True)
            logging.info("config dir [{}] created".format(config_dir) )
            
        config_filename = os.path.join(config_dir, 'pubchem.conf') 
                        
        try: 
            self.pubchem_config = ConfigParser()
            #~ self.pubchem_config.read(config_filename
            with open(config_filename) as config_file:
                self.pubchem_config.read_file(config_file)
            
        except FileNotFoundError as err:
            sys.stderr.write("ERROR: no config file - {}\n".format(err) )
            cache_path_missing = True
            
        try:
            self.pubchem_cache = self.pubchem_config['Downloads']['cache']
        except NoSectionError as err :
            sys.stderr.write("ERROR: Please add section in your LARAsuite/PubChem config file: {}\n".format(err) )
            cache_path_missing = True
        except NoOptionError as err:
            sys.stderr.write("ERROR(no optinon): Cannot read config file option in {}\n".format(err) )
            cache_path_missing = True
        except KeyError as err:
            sys.stderr.write("ERROR(key missing): Cannot read config file option in {}\n".format(err) )
            cache_path_missing = True
            
        if cache_path_missing :
            self.pubchem_config['Downloads'] = {}
            self.pubchem_config['Downloads']['cache'] = self.pubchem_cache_default
            self.pubchem_cache = self.pubchem_cache_default

            with open(config_filename, 'w') as configfile:
                self.pubchem_config.write(configfile)
                
        logging.debug("pubchem cache : {}".format(self.pubchem_cache) )
        
        if not os.path.exists(self.pubchem_cache):
            os.makedirs(self.pubchem_cache, exist_ok=True)

class PubChem:
    """ Class doc """
    def __init__ (self):
        """ Class initialiser """
    
    def requestPUG(self, identifier, namespace='cid', domain='compound', 
                operation=None, output='JSON', searchtype=None, **kwargs):
        """
        Construct API request from parameters and return the response.
    
        Full specification at http://pubchem.ncbi.nlm.nih.gov/pug_rest/PUG_REST.html
        """
        text_types = str, bytes
        
        if not identifier:
            raise ValueError('identifier/cid cannot be None')
        # If identifier is a list, join with commas into string
        if isinstance(identifier, int):
            identifier = str(identifier)
        if not isinstance(identifier, text_types):
            identifier = ','.join(str(x) for x in identifier)
        # Filter None values from kwargs
        kwargs = dict((k, v) for k, v in kwargs.items() if v is not None)
        
        # Build API URL
        urlid, postdata = None, None
        if namespace == 'sourceid':
            identifier = identifier.replace('/', '.')
        if namespace in ['listkey', 'formula', 'sourceid'] \
                or searchtype == 'xref' \
                or (searchtype and namespace == 'cid') or domain == 'sources':
            urlid = quote(identifier.encode('utf8'))
        else:
            postdata = urlencode([(namespace, identifier)]).encode('utf8')
        comps = filter(None, [API_BASE, domain, searchtype, namespace, urlid, operation, output])
        apiurl = '/'.join(comps)
        if kwargs:
            apiurl += '?{}'.format(urlencode(kwargs))
        # Make request
        try:
            sys.stderr.write('Request URL: %s\n' % apiurl)
            sys.stderr.write('Request data: %s\n' % postdata)
            
            response = urlopen(apiurl, postdata)
            return response
        except HTTPError as err:
            raise PubChemHTTPError(err)
            
    def get(self, identifier, pugview=True, namespace='cid', domain='compound', 
            operation=None, output='JSON', searchtype=None, **kwargs):
        """Request wrapper that automatically handles async requests."""
        if pugview:
            if (searchtype and searchtype != 'xref') or namespace in ['formula']:
                response = self.requestPUG(identifier, namespace, domain, None, 'JSON', searchtype, **kwargs).read()
                status = json.loads(response.decode())
                if 'Waiting' in status and 'ListKey' in status['Waiting']:
                    identifier = status['Waiting']['ListKey']
                    namespace = 'listkey'
                    while 'Waiting' in status and 'ListKey' in status['Waiting']:
                        time.sleep(2)
                        response = self.requestPUG(identifier, namespace, domain, operation, 'JSON', **kwargs).read()
                        status = json.loads(response.decode())
                    if not output == 'JSON':
                        response = self.requestPUG(identifier, namespace, domain, operation, output, searchtype, **kwargs).read()
            else:
                response = self.requestPUG(identifier, namespace, domain, operation, output, searchtype, **kwargs).read()
            return response
            
        else :
            if (searchtype and searchtype != 'xref') or namespace in ['formula']:
                response = self.requestPUG(identifier, namespace, domain, None, 'JSON', searchtype, **kwargs).read()
                status = json.loads(response.decode())
                if 'Waiting' in status and 'ListKey' in status['Waiting']:
                    identifier = status['Waiting']['ListKey']
                    namespace = 'listkey'
                    while 'Waiting' in status and 'ListKey' in status['Waiting']:
                        time.sleep(2)
                        response = self.requestPUG(identifier, namespace, domain, operation, 'JSON', **kwargs).read()
                        status = json.loads(response.decode())
                    if not output == 'JSON':
                        response = self.requestPUG(identifier, namespace, domain, operation, output, searchtype, **kwargs).read()
            else:
                response = self.requestPUG(identifier, namespace, domain, operation, output, searchtype, **kwargs).read()
            return response
    

    def get_json(self, identifier, namespace='cid', domain='compound', operation=None, searchtype=None, **kwargs):
        """Request wrapper that automatically parses JSON response and supresses NotFoundError."""
        try:
            return json.loads(self.get(identifier, namespace, domain, operation, 'JSON', searchtype, **kwargs).decode())
        except NotFoundError as err:
            log.info(err)
            return None

class PubChemError(Exception):
    """Base class for all PubChemPy exceptions."""
    pass
    
class PubChemHTTPError(PubChemError):
    """Generic error class to handle all HTTP error codes."""
    def __init__(self, e):
        self.code = e.code
        self.msg = e.reason
        try:
            self.msg += ': %s' % json.loads(e.read().decode())['Fault']['Details'][0]
        except (ValueError, IndexError, KeyError):
            pass
        if self.code == 400:
            raise BadRequestError(self.msg)
        elif self.code == 404:
            raise NotFoundError(self.msg)
        elif self.code == 405:
            raise MethodNotAllowedError(self.msg)
        elif self.code == 504:
            raise TimeoutError(self.msg)
        elif self.code == 501:
            raise UnimplementedError(self.msg)
        elif self.code == 500:
            raise ServerError(self.msg)

    def __str__(self):
        return repr(self.msg)

class BadRequestError(PubChemHTTPError):
    """Request is improperly formed (syntax error in the URL, POST body, etc.)."""
    def __init__(self, msg='Request is improperly formed'):
        self.msg = msg

class NotFoundError(PubChemHTTPError):
    """The input record was not found (e.g. invalid CID)."""
    def __init__(self, msg='The input record was not found'):
        self.msg = msg

class MethodNotAllowedError(PubChemHTTPError):
    """Request not allowed (such as invalid MIME type in the HTTP Accept header)."""
    def __init__(self, msg='Request not allowed'):
        self.msg = msg

class TimeoutError(PubChemHTTPError):
    """The request timed out, from server overload or too broad a request.

    See :ref:`Avoiding TimeoutError <avoiding_timeouterror>` for more information.
    """
    def __init__(self, msg='The request timed out'):
        self.msg = msg

class UnimplementedError(PubChemHTTPError):
    """The requested operation has not (yet) been implemented by the server."""
    def __init__(self, msg='The requested operation has not been implemented'):
        self.msg = msg

class ServerError(PubChemHTTPError):
    """Some problem on the server side (such as a database server down, etc.)."""
    def __init__(self, msg='Some problem on the server side'):
        self.msg = msg
