"""
________________________________________________________________________

:PROJECT: LARA

*chemspider.py - a chemistry information retrieval module*

:details: This module retrieves chemical data from various databases
         - 
:file:    chemspider.py
:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20190307
:date: (last modification) 20190307

.. note:: -
.. todo:: - #    

________________________________________________________________________
"""
__version__ = "0.0.1"

import logging

from chemspipy import ChemSpider
from os.path import expanduser
import configparser  # !!! renamed in python3.0 !!!


class SubstanceReaderMerck():
    """ Class doc """
    def __init__ (self):
        """ Class initialiser """
        pass            

    def sigmaAldrichCompound(self,vendor="aldrich", productID="407267", language="de", region="DE" ):    
    
        substance_properties = {}
        substance_name_xpath = '//h1[@itemprop="name"]'
        substance_description_xpath = '//h2[@itemprop="description"]'
        synonymes_xpath = '//p[@class="synonym"]/strong' # or: synonymes_xpath = '//*[@id="productDetailHero"]/div/div[1]/p/strong/text()'
        sumformula_xpath = '//*/p[text()="Linear Formula "]/span'
        mwt_xpath = '//*/p[text()="Molecular Weight "]/span'
        density_xpath = '//*/p[text()="Molecular Weight "]/span'
        beilstein_reg_no_xpath = '//*/p[text()="Beilstein Registry Number "]/span'
        substance_description_xpath = '//h2[@itemprop="description"]'
        cas_xpath = '//*/p[text()="CAS Number "]/span/a' # or: cas_xpath = '//*[@id="productDetailHero"]/div/div[1]/ul/li[1]/p/span/a/text()'
        png_xpath = '//*[@class="image prodImage"]/a'  #'//*[@id="productDetailHero"]/div/div[2]/div[1]/a/img'
        PubChemID_xpath = u'//*/p[text()="\u00A0PubChem Substance ID "]/span'
        NMRspectrum_url = 'http://www.sigmaaldrich.com/spectra/fnmr/FNMR003957.PDF'
        spec_sheet_xpath = ""
        properties_refractive_index_xpath = 'Eigenschaften'
        
        url_request = 'http://www.sigmaaldrich.com/catalog/product/{vendor}/{productID}?lang={language}&region={region}'.format(vendor=vendor, 
                                                                                                                           productID=productID, 
                                                                                                                           language=language, region=region)
        page = requests.get(url_request)
        tree = html.fromstring(page.content)
            
        if tree.xpath(substance_name_xpath): substance_properties['name'] = tree.xpath(substance_name_xpath).pop().text_content().strip()
        if tree.xpath(substance_description_xpath) : substance_properties['description'] = tree.xpath(substance_description_xpath).pop().text_content().strip().replace(u"\u2265",u">= ")
        if tree.xpath(synonymes_xpath): 
            synonymes = tree.xpath(synonymes_xpath).pop().text_content().split(",")
            substance_properties['synonymes'] = [ syn.strip() for syn in synonymes] 
        #substance_class = models.ForeignKey(Item_class, related_name='substance_class', blank=True, null=True) # salt, complex, nucleotide
        #substance_function = models.ForeignKey(Item_class, related_name='substance_function', blank=True) # educt, product, solvent 
        if tree.xpath(sumformula_xpath): substance_properties['sumformula'] = tree.xpath(sumformula_xpath).pop().text_content().strip()
        if tree.xpath(mwt_xpath): substance_properties['mwt'] = tree.xpath(mwt_xpath).pop().text_content().strip() # g / mol
        #substance_properties['density'] = tree.xpath(density_xpath).pop().text_content() # g / cm3
        #substance_properties['abs_coeff'] = tree.xpath(abs_coeff_xpath).pop().text_content()
        #substance_properties['melting_point'] = tree.xpath(melting_point_xpath).pop().text_content() # in C
        #substance_properties['boiling_point'] = tree.xpath(boiling_point_xpath).pop().text_content() # in C
        #properties =  # JSON details (e.g. more physical data)
        substance_properties['vendor'] = vendor
        substance_properties['vendor_order_number'] = productID
        # substance_properties['vendor_price'] =  # in Euro
        # ordered_by  storage_location  hazardous_classes precaution_classes hazardous_symbols storage_class remarks
        if tree.xpath(PubChemID_xpath): substance_properties['PubChemID'] = tree.xpath(PubChemID_xpath).pop().text_content().strip()
        if tree.xpath(cas_xpath): substance_properties['CAS'] = tree.xpath(cas_xpath).pop().text_content()
        
        #print substance_properties
        
        #~ r = requests.get(file_png_url, stream=True)
        #~ if r.status_code == 200:
            #~ substance_png = []
            #~ for chunk in r.iter_content(1024):
                #~ substance_png += chunk
            #~ substance_properties['png'] = substance_png 
        
        #~ # retrieve data from db
        #~ out_filename = "spec_sheet.pdf"
        #~ r = requests.get(NMRspectrum_url, stream=True)
        #~ if r.status_code == 200:
            #~ with open(out_filename, 'wb') as f:
                #~ for chunk in r.iter_content(1024):
                    #~ f.write(chunk)
                    
        return(substance_properties)
 
