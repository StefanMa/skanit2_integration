"""
________________________________________________________________________

:PROJECT: LARA

*substance_prop_calc.py - a chemistry information retrieval module*

:details: This module retrieves chemical data from various databases
         - 
:file:    chemspider.py
:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20190310
:date: (last modification) 20190310

.. note:: -
.. todo:: - #    

________________________________________________________________________
"""
__version__ = "0.0.1"

import logging

import sys
import pybel
import openbabel

class SubstancePropCalculator():
    """ Class doc """
    def __init__ (self):
        """ Class initialiser """
        
    def calcSubstanceProperties(self, smiles="", ):
        """ :param [param_name]: [description]"""
        
        self.smiles = smiles
        
        try:
            self.mol = pybel.readstring("smi",self.smiles)
            
            # calculate descriptors
            self.descs = self.mol.calcdesc()
            
            # generate 2D coordinates, needs openbabel -> MDL data
            obConversion = openbabel.OBConversion()
            obConversion.SetInAndOutFormats("smi", "mdl")
            
            self.obmol = openbabel.OBMol()
            obConversion.ReadString(self.obmol, self.smiles)
            
            self.gen2d = openbabel.OBOp.FindType("gen2d")
            self.gen2d.Do(self.obmol)
            
            self.MDL = obConversion.WriteString(self.obmol)
            
        except Exception as err:
            sys.stderr.write("OpenBabel/pybel Error: {}".format(err) )
            
        return 
