"""
________________________________________________________________________

:PROJECT: LARA

*wikipedia_substance.py - a chemistry information retrieval module*

:details: This module retrieves chemical data from wikipedia
         - 
:file:    chemspider.py
:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20190307
:date: (last modification) 20190320

.. note:: -
.. todo:: - #    

________________________________________________________________________
"""
__version__ = "0.0.1"

import logging

from os.path import expanduser


            
