"""_____________________________________________________________________

:PROJECT: lara_library

*lara_library unittests*

:details: lara_library chem unittests

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20190320
:date: (last modification) 20190320

.. note:: -
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.0.1"

import os
import unittest
import logging
import laralib.chem.pubchem as pc

TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), "test_data")

class TestLARAlibChem(unittest.TestCase):
    """ Class doc """
    def setUp(self):
        """ setting up test environment
            :param [param_name]: [description]"""
        
        logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
        #~ logging.debug("is cl_obj 2 still available ? \n Yes- para: {}\n".format(cl_obj2.parameters) )
         
        self.pc_reader = pc.SubstanceReaderPubChem()
    
    @unittest.skip("skipping PubChem")            
    def test_substanceReaderPubChem(self):
        """ Evaluate class tests
            :param [param_name]: [description]"""
            
        substance_name = "Formaldehyde"
        
        self.pc_reader.pubChemCompoundByName( substance_name=substance_name, acronym='', record_type='3d' )
        
        #~ logging.debug("substance json: {}".format(subst_json) )
        
        #~ self.assertTrue( lr.layout_description == "Test Layout 1" )
        
        # check, how to access the objects    
        #~ logging.debug("param still exist ? ext cl_ob 1 para is: {}".format(cl_obj1.parameters) )
        
if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

    unittest.main()


