"""_____________________________________________________________________

:PROJECT: lara_library

*lara_library unittests*

:details: lara_library unittests

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20190113
:date: (last modification) 20190113

.. note:: -
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.0.3"

import os
import unittest
import logging

from laralib.container.layout_reader import LayoutReader


TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), "test_data")

@unittest.skip("skipping container / layout reading")
class TestLARAlib(unittest.TestCase):
    """ Class doc """
    def setUp(self):
        """ setting up test environment
            :param [param_name]: [description]"""
        
        logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
         #~ logging.debug("is cl_obj 2 still available ? \n Yes- para: {}\n".format(cl_obj2.parameters) )
         
        layout_filename = "0003_plate_layout_template_96well_fixed_well_volume.csv" # "VA18121401_plate_layout_dye_spectra.csv" #
    
        self.layout_filename = os.path.join(TEST_DATA_DIR, layout_filename)
        
    def test_LayoutReader(self):
        """ Evaluate class tests
            :param [param_name]: [description]"""
            
        lr = LayoutReader(self.layout_filename)
        
        logging.debug("layout_description {}".format(lr.layout_description) )
        
        self.assertTrue( lr.layout_description == "Test Layout 1" )
        
        self.assertTrue( lr.nrows == 8 )
        self.assertTrue( lr.ncols == 12 )
        
        self.assertTrue( lr.volumes == 211 )
        
        self.assertTrue( lr.conc_unit == "uM" )
        self.assertTrue( lr.vol_unit == "ul" )
             
        # check, how to access the objects    
        #~ logging.debug("param still exist ? ext cl_ob 1 para is: {}".format(cl_obj1.parameters) )
        
if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)

    unittest.main()


