"""_____________________________________________________________________

:PROJECT: lara_library

*lara_library unittests*

:details: lara_library unittests

:authors: mark doerr (mark@uni-greifswald.de)

:date: (creation)          20190113
:date: (last modification) 20190113

.. note:: -
.. todo:: - 
________________________________________________________________________

**Copyright**:
  This file is provided "AS IS" with NO WARRANTY OF ANY KIND,
  INCLUDING THE WARRANTIES OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.

  For further Information see LICENSE file that comes with this distribution.
________________________________________________________________________
"""

__version__ = "0.0.3"

import os
import unittest
import logging

from laralib.eval.evaluation import Evaluate
import laralib.eval.safe_exec as se 

from laralib.projects.projects import ProjectAbstr, ExperimentAbstr

TEST_DATA_DIR = os.path.join(os.path.dirname(__file__), "test_data")

#~ SECRET_KEY = 'MY SECRET KEY'
#~ import django

class Project(ProjectAbstr):
    """ Class doc """
    def addDB (self):
        logging.debug("add db implementation {}".format(self.name) )
        
class TestLARAlibProjects(unittest.TestCase):
    """ Class doc """
    def setUp(self):
        """ setting up test environment
            :param [param_name]: [description]"""
        
        logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
        #~ logging.debug("is cl_obj 2 still available ? \n Yes- para: {}\n".format(cl_obj2.parameters) )
        #~ os.environ.setdefault("DJANGO_SETTINGS_MODULE", "lara_django.lara.settings.devel")
        #~ django.setup()

        project_struct_def_file = "lara_test_project_proj_struct.py"
        self.ext_progr_filename = os.path.join(TEST_DATA_DIR, "eval_test_script.py")
        self.project_descr_filename = os.path.join(TEST_DATA_DIR, project_struct_def_file)

    def test_Project(self):
        """ Evaluate class tests
            :param [param_name]: [description]"""
            
        with open(self.project_descr_filename, 'r') as proj_file:
            exec(proj_file.read(), globals())
            #~ save_exec(ext_progr.read())
    
    @unittest.skip("skipping evaluation")        
    def test_Evaluate(self):
        """ Evaluate class tests
            :param [param_name]: [description]"""
            
        with open(self.ext_progr_filename, 'r') as ext_progr:
            exec(ext_progr.read(), globals())
            #~ save_exec(ext_progr.read())
        
        # check, how to access the objects    
        logging.debug("param still exist ? ext cl_ob 1 para is: {}".format(cl_obj1.parameters) )
        
if __name__ == '__main__':
    #~ logging.basicConfig(format='%(levelname)s| %(module)s.%(funcName)s:%(message)s', level=logging.DEBUG)
    #~ logging.basicConfig(format='%(levelname)s|%(module)s.%(funcName)s:%(message)s', level=logging.ERROR)
    #~ unittest.main()
    pass

